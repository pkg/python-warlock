python-warlock (1.3.3-2apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 06:47:47 +0000

python-warlock (1.3.3-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 12:05:34 +0200

python-warlock (1.3.3-1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: 4.5.0.
  * Fix running tests.
  * Add python3-pytest-cov as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 09:59:37 +0200

python-warlock (1.2.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Removed Python 2 support.
  * Standards-Version: 4.4.0.

 -- Thomas Goirand <zigo@debian.org>  Sat, 20 Jul 2019 21:17:22 +0200

python-warlock (1.2.0-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to sdk

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 01:55:20 +0000

python-warlock (1.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 20:48:10 +0000

python-warlock (1.2.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol
  * d/control: Set Vcs-* to salsa.debian.org

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Standards-Version is now 4.1.3.
  * Using pkgos-dh_auto_install.
  * Switched to pytest, as upstream did so.
  * Checking for DEB_BUILD_OPTIONS=nocheck.
  * Using github tag over HTTPS for debian/watch.
  * Fixed debian/copyright ordering and years.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Feb 2018 08:13:21 +0000

python-warlock (1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Added extend-diff-ignore = "^[^/]*[.]egg-info/" in debian/source/options.
  * Standards-Version is now 3.9.6 (no change).
  * Drops debian/patches/python3.patch applied upstream.
  * Fixed watch file to use the pypi redirector.
  * Fixed long desc.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2015 16:51:38 +0200

python-warlock (1.0.1-2) unstable; urgency=low

  * Cleans correctly (package can build twice).
  * Adds python3 support (Closes: #717469).
  * Relicenses the packaging using Apache-2.0 (with prior agreement from
    Canonical, see #717469).

 -- Thomas Goirand <zigo@debian.org>  Wed, 24 Jul 2013 15:01:57 +0800

python-warlock (1.0.1-1) unstable; urgency=low

  * New upstream release.
  * Ran wrap-and-sort.
  * Standard-Version: is now 3.9.4.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 May 2013 15:47:07 +0800

python-warlock (1.0.0-1) unstable; urgency=low

  * Now upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 11 May 2013 06:50:16 +0000

python-warlock (0.8.1-1) experimental; urgency=low

  * Initial release (Closes: #687510).

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Oct 2012 11:55:15 +0000
